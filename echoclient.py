from twisted.internet import reactor, protocol

class EchoClient(protocol.Protocol):

    def connectionMade(self):
        self.transport.write("Hello, World!")

    def dataReceived(self, data):
        print"Server said:", data
        self.transport.loseConnection()

class EchoFactory(protocol.ClientFactory):

    def buildProtocol(self, addr):
        return EchoClient()

    def clientConnectionfailed(self, connector, reason):
        print("Connection failed.")
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        print("Connection lost.")
        reactor.stop()


reactor.connectTCP("127.0.0.1", 1234, EchoFactory())
reactor.run()