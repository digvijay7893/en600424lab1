from twisted.protocols import basic
import os
from twisted.internet import protocol, reactor
from playground.twisted.endpoints import GateServerEndpoint

class HTTP_Server(basic.LineReceiver):
    
    def __init__(self):
        self.lines = None
        self.l_final = list()

    def dataReceived(self, line):
        
        self.lines = line

        print self.lines

        self.lines = self.lines.split()

        for i in range(0, len(self.lines)):
            if self.lines[i] == '\\r\\n':
                break
            else:
                self.l_final.append(self.lines[i])
            

        self.sendResponse()


    def sendResponse(self):
        temp = self.l_final;

        if temp[1] == '/':
            self.transport.write('\n\nDEFAULT')
        
        elif (temp[0] == 'GET') and (temp[2] == 'HTTP/1.0' or temp[2] == 'HTTP/1.1'):
            
            if os.path.exists(temp[1]):
                self.transport.write('\n200 OK\r\n\r\n' + open(temp[1], 'r').read())
            else:
                self.transport.write('\n404 Resource + temp[1]' + ' not found')
        else:
            self.transport.write('\n400 Syntax Error (in GET or HTTP)')                                  
        
        self.transport.loseConnection()

class HTTP_Server_Factory(protocol.ServerFactory):
    def buildProtocol(self, addr):
        return HTTP_Server()


endpoint = GateServerEndpoint.CreateFromConfig(reactor, 101, 'gatekey1')
endpoint.listen(HTTP_Server_Factory())

reactor.run()
