from twisted.protocols import basic
from twisted.internet import protocol, reactor
from playground.twisted.endpoints import GateClientEndpoint

class HTTP_Client(basic.LineReceiver):
    
    def __init__(self, factory):
        self.factory = factory

    def connectionMade(self):

        push1 = raw_input("Enter the file to be retrieved: \n")
        push2 = raw_input("Enter any other data to be sent to server: \n")
        push_1 = 'GET ' + push1 + ' HTTP/1.0\r\n'
        push_final = push_1 + ' ' + push2

        self.transport.write(push_final)
        
            
    def dataReceived(self, data):
        print data
        self.transport.loseConnection()
        reactor.stop()

    def clientConnectionfailed(self, connector, reason):
        print("Connection failed.")

    def clientConnectionLost(self, connector, reason):
        print("Connection lost.")

class HTTP_Client_Factory(protocol.ClientFactory):
    def buildProtocol(self, addr):
        return HTTP_Client(self)

endpoint = GateClientEndpoint.CreateFromConfig(reactor, '20164.0.0.1', 101, 'gatekey2')
endpoint.connect(HTTP_Client_Factory())

reactor.run()
